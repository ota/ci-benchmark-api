#!/usr/bin/env python
import os
import sys
import re

activate_this = os.path.dirname(__file__) + '/bin/activate_this.py'
if os.path.exists(activate_this):
   execfile(activate_this, dict(__file__=activate_this))

def set_env(content):
   for line in content.splitlines():
      m1 = re.match(r'\A([A-Za-z_0-9]+)=(.*)\Z', line)
      if m1:
         key, val = m1.group(1), m1.group(2)

         m2 = re.match(r"\A'(.*)'\Z", val)
         if m2:
            val = m2.group(1)

         m3 = re.match(r'\A"(.*)"\Z', val)
         if m3:
            val = re.sub(r'\\(.)', r'\1', m3.group(1))
         if not os.environ.has_key(key):
            os.environ[key] = val

env_file = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(env_file):
   set_env(open(env_file, 'r').read())

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cibenchmark.settings")

    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
