from cibenchmark.settings import *


DEBUG = False
TEMPLATE_DEBUG = False

STATIC_ROOT = '/usr/local/ripple/cibenchmark.ripplemotion.fr/shared/public/static/'
MEDIA_ROOT = '/usr/local/ripple/cibenchmark.ripplemotion.fr/shared/public/media/'

MEDIA_URL = "http://cibenchmark.ripplemotion.fr/media/"

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

CELERY_ALWAYS_EAGER = False

RAVEN_CONFIG['dsn'] = 'http://29232013849c4320bec48d21baa49424:2cfc0ebcfcad4f28965d0868e3d564a8@sentry.ripplemotion.fr/1' # Change according to project

# Uncomment the next line if you're using SSL
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')