# Django settings for cibenchmark project.

import os

PROJ_DIR = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
   ('Support', 'support@ripplemotion.fr'),
)

MANAGERS = ADMINS

DATABASES = {
   'default': {
      'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
      'NAME': 'cibenchmark',                 # Or path to database file if using sqlite3.
      'USER': os.environ.get('PGUSER', 'ripple'),                 # Not used with sqlite3.
      'PASSWORD': os.environ.get('PGPASSWORD', 'ripple'),              # Not used with sqlite3.
      'HOST': os.environ.get('PGHOST', ''),                 # Set to empty string for localhost. Not used with sqlite3.
      'PORT': os.environ.get('PGPORT', '5432'),                 # Set to empty string for default. Not used with sqlite3.
   }
}

ALLOWED_HOSTS = [".ripplemotion.fr"]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.expanduser('~/.cibenchmark/media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
# MEDIA_URL = '/media/'
MEDIA_URL = "http://localhost:8000/media/"
# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.expanduser('~/.cibenchmark/static/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
   # Put strings here, like "/home/html/static" or "C:/www/django/static".
   # Always use forward slashes, even on Windows.
   # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
   'django.contrib.staticfiles.finders.FileSystemFinder',
   'django.contrib.staticfiles.finders.AppDirectoriesFinder',
   # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'zv3#&amp;prn6-3#5d=2yd626qr4k-iw*@)1r=3)d9ltk*dv)%*3jz'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
   'django.template.loaders.filesystem.Loader',
   'django.template.loaders.app_directories.Loader',
   # 'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
   'django.middleware.common.CommonMiddleware',
   'django.contrib.sessions.middleware.SessionMiddleware',
   'django.middleware.csrf.CsrfViewMiddleware',
   'django.contrib.auth.middleware.AuthenticationMiddleware',
   'django.contrib.messages.middleware.MessageMiddleware',
   # Uncomment the next line for simple clickjacking protection:
   # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'cibenchmark.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'cibenchmark.wsgi.application'

TEMPLATE_DIRS = (
   # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
   # Always use forward slashes, even on Windows.
   # Don't forget to use absolute paths, not relative paths.
   os.path.join(PROJ_DIR, 'templates'),
)

INSTALLED_APPS = (
   'django.contrib.auth',
   'django.contrib.contenttypes',
   'django.contrib.sessions',
   'django.contrib.sites',
   'django.contrib.messages',
   'django.contrib.staticfiles',
   # Uncomment the next line to enable the admin:
   'django.contrib.admin',
   # Uncomment the next line to enable admin documentation:
   # 'django.contrib.admindocs',
   'south',
   'django_extensions',
   'devserver',
   'raven.contrib.django',
   'gunicorn',
   'djcelery',
   'benchmarks',
)

import djcelery
djcelery.setup_loader()
CELERY_ALWAYS_EAGER = True #for dev only
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
BROKER_URL = "amqp://ripple:0ripple1@localhost:5672/cibenchmark"
CELERY_DEFAULT_DELIVERY_MODE = 'transient' #fast switch

RAVEN_CONFIG = {
    'auto_log_stacks': True,
}

LOGGING = {
   'version': 1,
   'disable_existing_loggers': True,
   'root': {
      'level': 'WARNING',
      'handlers': ['console', 'sentry'],
   },
   'formatters': {
      'verbose': {
         'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
      },
   },
   'handlers': {
      'sentry': {
         'level': 'ERROR',
         'class': 'raven.contrib.django.handlers.SentryHandler',
      },
      'console': {
         'level': 'DEBUG',
         'class': 'logging.StreamHandler',
         'formatter': 'verbose'
      }
   },
   'loggers': {
      'django.db.backends': {
         'level': 'ERROR',
         'handlers': ['console'],
         'propagate': False,
      },
      'raven': {
         'level': 'DEBUG',
         'handlers': ['console'],
         'propagate': False,
      },
      'sentry.errors': {
         'level': 'DEBUG',
         'handlers': ['console'],
         'propagate': False,
      },
   },
}

CACHES = {
   'default': {
      'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
      'LOCATION': '127.0.0.1:11211',
   }
}


if os.path.exists('/opt/local/lib/libgeos_c.dylib'):
   GEOS_LIBRARY_PATH =  '/opt/local/lib/libgeos_c.dylib'

if os.path.exists('/opt/local/lib/libgdal.dylib'):
   GDAL_LIBRARY_PATH =  '/opt/local/lib/libgdal.dylib'
