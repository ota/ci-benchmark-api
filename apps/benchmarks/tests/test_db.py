from django.test import TestCase
from django.db import models
from django.contrib.gis.geos import GEOSGeometry

class ZipcodeModelTestCase(TestCase):
   model = models.get_model('benchmarks', 'ZipCode')
   def test_insert(self):
      self.model.objects.create(
         poly=GEOSGeometry('POLYGON(( 10 10, 10 20, 20 20, 20 15, 10 10))', srid=3084),
         code=1234
      )
   