from django.contrib.gis.db import models

# Create your models here.
class Zipcode(models.Model):
   """(Zipcode description)"""
   poly = models.PolygonField(srid=4326)
   code = models.CharField(max_length=100)
   

   def __unicode__(self):
      return u"Zipcode"
