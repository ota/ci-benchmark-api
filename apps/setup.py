from setuptools import setup, find_packages

setup(
    name='cibenchmark-server',
    version='1.0',
    long_description="",
    packages= find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires = []
)