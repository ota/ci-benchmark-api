#/bin/bash
set -e
set -x

sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable 
sudo apt-get update
sudo apt-get install libgdal-dev postgresql-9.1-postgis-2.1

#create postgres database
createuser -U postgres -s ripple
createdb -U ripple cibenchmark
psql -c 'CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;' -U ripple -d cibenchmark


#install RabbitMQ

wget --quiet -O - https://gist.githubusercontent.com/marcqualie/4745097/raw/eee21438dc96b22be02e5040ff7081ce03e9a434/install.rabbitmq.sh | sudo /bin/bash

#install python app
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal

pip install -r requirements.txt
pip install -e apps/


#run tests
./manage.py test --noinput apps

