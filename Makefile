buildout:
	[ -e bin/python ] || virtualenv .
	./bin/pip install -r requirements.txt
	./bin/pip install -e apps/
	./bin/pip install -r deploy_requirements.txt

test:
	./manage.py test --noinput apps

clean:
	find . -name "*.pyc" | xargs rm -rf
	find . -name "*.pyo" | xargs rm -rf

realclean: clean
	find . -name "*.egg-info" | xargs rm -rf
	rm -rf .Python bin include lib share src
