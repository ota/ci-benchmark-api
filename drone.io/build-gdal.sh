#!/bin/bash
set -e
set -x


sudo apt-get install build-essential python-all-dev
pushd /tmp
wget http://download.osgeo.org/gdal/gdal-1.9.0.tar.gz
tar xfz gdal-1.9.0.tar.gz
pushd gdal-1.9.0
./configure --with-python
make -j4 > /dev/null
sudo make install > /dev/null
popd
popd
