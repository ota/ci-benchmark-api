try:
   from fabric.api import *
   from deploy import *
except ImportError:
   raise Exception("Failed to load ripple-deploy. You are probably missing\n\t ./bin/pip install -r deploy_requirements.txt" )


env.env_file = ''
env.procfile = 'Procfile'
env.app_name = 'cibenchmark.ripplemotion.fr'

env.db_url = 'postgres://ripple@localhost:5432/cibenchmark'
env.db_replica_url = ''
env.broker_url = 'amqp://ripple:0ripple1@localhost:5672/cibenchmark'
env.vpn_url = 'pptp://ripple.server:kooks3)bool@office.ripplemotion.fr/VPN-Ripple'
env.new_relic_license = ''
env.new_relic_config = None


def dev():
   """
   configure hosts to target dev environment
   """
   env.user = 'ripple'
   env.password = 'ripple'
   env.app_name = 'cibenchmark-dev.ripplemotion.fr'
   env.hosts = ['', ]
   env.api_master = ''
   env.env_file = 'integration.env'
   env.root = '/usr/local/ripple/%s/' % env.app_name
   env.nginx_file = 'conf/nginx/%s.conf' % env.app_name
   env.logrotate_file = 'conf/logrotate/%s.conf' % env.app_name


def prod():
   """
   configure hosts to target prod environment
   """
   env.user = 'ripple'
   env.password = ''
   env.app_name = 'cibenchmark.ripplemotion.fr'
   env.hosts = ['', ]
   env.api_master = ''
   env.env_file = 'production.env'
   env.root = '/usr/local/ripple/%s/' % env.app_name
   env.nginx_file = 'conf/nginx/%s.conf' % env.app_name
   env.logrotate_file = 'conf/logrotate/%s.conf' % env.app_name
   env.ssl = {
      'certificate': 'conf/certificates/cert-ripplemotion.fr.crt',
      'private_key': 'conf/certificates/key-ripplemotion.fr.key',
      'authority': 'conf/certificates/GandiStandardSSLCA.pem'
   }
