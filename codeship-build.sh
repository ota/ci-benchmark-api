#/bin/bash
set -e
set -x

createuser -U postgres -s ripple
createdb -U ripple cibenchmark
psql -c 'CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;' -U ripple -d cibenchmark


export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal

pip install -r requirements.txt
pip install -e apps/


#create database
./manage.py syncdb --migrate --noinput


